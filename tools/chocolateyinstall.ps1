$ErrorActionPreference = 'Stop';

$toolsDir = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"

$pkgVersion = '2.1.6'
$pkgChecksum = '1C4DBAAB7BBFBC111F2C2C73321E67A3E948CA60E1E1E04F7381DBAD1D75D273'

$packageArgs = @{
  packageName   = $env:ChocolateyPackageName
  unzipLocation = $toolsDir
  url           = "http://www.cuetools.net/install/CUETools_$pkgVersion.zip"
  checksum      = $pkgChecksum
  checksumType  = 'sha256'
  softwareName  = 'CUETools*'
  validExitCodes= @(0)
}

Install-ChocolateyZipPackage @packageArgs

$pgkExeFolder = "$toolsDir\CUETools_$pkgVersion"

$files = get-childitem $pgkExeFolder -include *.exe -recurse
foreach ($file in $files) {
  New-Item "$file.ignore" -type file -force | Out-Null
}

@('CUETools', 'CUERipper') | ForEach-Object {
    New-Item "$($toolsDir)\$_.exe.gui" -Type File -Force | Out-Null
    $shortcutPath = Join-Path $([Environment]::GetFolderPath([System.Environment+SpecialFolder]::CommonPrograms)) "CUETools\$_.lnk"
    if (-not (Test-Path $shortcutPath)) {
      Install-ChocolateyShortcut -ShortcutFilePath $shortcutPath -TargetPath "$pgkExeFolder\$_.exe"
    }
}
