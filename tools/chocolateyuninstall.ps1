$ErrorActionPreference = 'Stop'

@('CUETools', 'CUERipper') | ForEach-Object {
  $shortcutPath = Join-Path $([Environment]::GetFolderPath([System.Environment+SpecialFolder]::CommonPrograms)) "CUETools\$_.lnk"
  if (Test-Path $shortcutPath) {
    Remove-Item $shortcutPath
  }
}
